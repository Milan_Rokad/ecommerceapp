from django.urls import path, include, reverse_lazy
from django.contrib.auth import views as auth_views
from . import views
from django.conf.urls import url


app_name = 'accounts'

urlpatterns = [
    # social authentication --> facebook and google
    path('oauth/', include('social_django.urls',
                           namespace='socialauth'), name='socialauth'),
    # user profile page
    path('', views.myaccount, name='myaccount'),
    path('pastorder/', views.pastorder, name="pastorder"),

    # functionality --> login, logout, signup, account activation, assword reset
    path('signup/', views.signup, name='signup'),
    path('login/', auth_views.LoginView.as_view(), name='login'),
    path('logout/', views.logout_customized, name='logout'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views.activate, name='activate'),
    path('password_reset/', auth_views.PasswordResetView.as_view(template_name='custom/password_reset_form.html',
                                                                 email_template_name='custom/password_reset_email.html', success_url='/accounts/password_reset/done/'), name='password_reset'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.PasswordResetConfirmView.as_view(success_url=reverse_lazy('accounts:password_reset_complete')), name='password_reset_confirm'),
    url(r'^', include('django.contrib.auth.urls')),
]
