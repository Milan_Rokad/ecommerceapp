from django.contrib.auth import login, logout
from django.contrib.auth.models import User
from django.core.mail import EmailMessage
from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from django.contrib.auth.decorators import login_required

from orders.models import OrderItem
from product.models import Review

from .tokens import account_activation_token
from .UserCreationFormCustomized import UserCreationFormCustomized

allowed_host_addr = ['127.0.0.1:8000', 'ecomappmr1.herokuapp.com']


def signup(request):
    if request.method == 'POST':
        form = UserCreationFormCustomized(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            current_host = request.META['HTTP_HOST']
            if current_host in allowed_host_addr:
                mail_subject = 'Activate your account.'
                message = render_to_string('registration_activate_email.html', {
                    'user': user,
                    'domain': current_host,
                    'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                    'token': account_activation_token.make_token(user),
                })
                to_email = form.cleaned_data.get('email')
                email = EmailMessage(
                    mail_subject, message, to=[to_email]
                )
                email.send()
                user.save()
                return render(request, 'registration_activation_message.html')
    else:
        form = UserCreationFormCustomized()
    return render(request, 'signup.html', {'form': form})


def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        user.backend = 'django.contrib.auth.backends.ModelBackend'
        login(request, user)
        return HttpResponse('Your account has been activate successfully')
    else:
        return HttpResponse('Activation link is invalid!')


@login_required
def myaccount(request):
    user = User.objects.filter(id=request.user.id).values()
    return render(request, 'myaccount.html', {'userx': user})


@login_required
def pastorder(request):
    ordered = OrderItem.objects.filter(
        order__user_id=request.user.id).values('order_id', 'product__code', 'product__name', 'quantity', 'price')
    pastreview = Review.objects.filter(user_id=request.user.id).values(
        'user', 'product', 'star', 'comment', 'created_at', 'updated_at', 'user__username', 'product__code')
    return render(request, 'pastorder.html', {'ordered': ordered, 'pastreview': pastreview})


@login_required
def logout_customized(request):
    x = request.session
    if 'cart' in x:
        cartdata = x['cart']
    else:
        cartdata = {}
    logout(request)
    request.session['cart'] = cartdata
    return redirect('product:product_list')
