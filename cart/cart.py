from django.conf import settings
from product.models import Product
from decimal import Decimal


class Cart(object):
    def __init__(self, request):
        self.session = request.session
        cart = self.session.get(settings.CART_SESSION_ID)
        if not cart:
            cart = self.session[settings.CART_SESSION_ID] = {}
        self.cart = cart

    def add(self, product, quantity):
        productcode = str(product.code)
        productprice = str(product.price)
        if productcode not in self.cart:
            self.cart[productcode] = {
                'quantity': 1, 'price': productprice}
        else:
            self.cart[productcode]['quantity'] = quantity
        self.save()

    def remove(self, product):
        productcode = str(product.code)
        if productcode in self.cart:
            del self.cart[productcode]
            self.save()

    def save(self):
        self.session[settings.CART_SESSION_ID] = self.cart
        self.session.modified = True

    def __iter__(self):
        productcodes = self.cart.keys()
        products = Product.objects.filter(code__in=productcodes)

        for product in products:
            self.cart[str(product.code)]['product'] = product

        for item in self.cart.values():
            item['price'] = int(item['price'])
            item['quantity'] = int(item['quantity'])
            item['total_price'] = item['price'] * item['quantity']
            yield item

    def __len__(self):
        return sum(1 for item in self.cart.keys())

    def get_total_price(self):
        return sum(Decimal(item['price']) * int(item['quantity']) for item in self.cart.values())

    def clear(self):
        del self.session[settings.CART_SESSION_ID]
        self.session.modified = True
