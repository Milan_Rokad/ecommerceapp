from django.db import models
from django.contrib.auth.models import User
from product.models import Product


class Cart(models.Model):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, blank=False, on_delete=models.CASCADE)
    quantity = models.IntegerField()
    objects = models.Manager()

    class Meta:
        db_table = 'cart'
