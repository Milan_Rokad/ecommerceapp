from django.shortcuts import render, get_object_or_404, redirect
from django.views.decorators.http import require_POST
from django.http import JsonResponse
from product.models import Product
from .cart import Cart


def cartlength(request):
    cartlength = len(Cart(request))
    return {'cartlength': cartlength}


def cart_detail(request):
    cart = Cart(request)
    return render(request, 'cart_detail.html', {'cartxx': cart})


@require_POST
def cart_add(request, productcode, quantity=1):
    req = request.POST
    if 'quantity' in req:
        quantity = req['quantity']
    product = get_object_or_404(Product, code=str(productcode))

    cart = Cart(request)
    cart.add(product=product, quantity=quantity)

    priceperone = cart.cart[str(productcode)]['price']
    totalprice = int(quantity) * int(priceperone)
    grandtotal = cart.get_total_price()
    cartlen = len(cart)

    if str(productcode) in request.session['cart'].keys():
        in_cart = True
    else:
        in_cart = False

    responseData = {'cartlen': cartlen, 'totalprice': totalprice,
                    'grandtotal': grandtotal, 'in_cart': in_cart}
    return JsonResponse(responseData)


def cart_remove(request, productcode):
    cart = Cart(request)
    product = get_object_or_404(Product, code=productcode)
    cart.remove(product)
    return redirect('cart:cart_detail')
