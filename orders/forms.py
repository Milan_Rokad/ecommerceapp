from django import forms
from .models import Order


class OrderCreateForm(forms.ModelForm):
    class Meta:
        model = Order
        exclude = ('user',)
        fields = ['first_name', 'last_name',  'mobile_number',
                  'address', 'postal_code', 'city', 'state']
