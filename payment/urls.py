# from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'payment'

urlpatterns = [
    url(r'^process/$', views.process_payment, name='process'),
    url(r'^done/$', views.payment_done, name='done'),
    url(r'^cancelled/$', views.payment_canceled, name='cancelled'),
    url(r'^charge/$', views.charge, name='charge'),
]
