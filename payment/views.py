import stripe
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import get_object_or_404
from django.conf import settings
from django.urls import reverse
from paypal.standard.forms import PayPalPaymentsForm
from orders.models import Order
# from django.views.generic.base import TemplateView


@csrf_exempt
def payment_done(request):
    return render(request, 'payment_done.html')


@csrf_exempt
def payment_canceled(request):
    return render(request, 'payment_cancelled.html')


def process_payment(request):
    order_id = request.session.get('order_id')
    order = get_object_or_404(Order, id=order_id)
    host = request.get_host()
    paypal_dict = {
        'business': settings.PAYPAL_RECEIVER_EMAIL,
        # 'amount': '%.2f' % order.price.quantize(Decimal('.01')),
        'amount': 100,
        'item_name': 'Order {}'.format(order.id),
        'invoice': str(order.id),
        'currency_code': 'INR',
        'notify_url': 'http://{}{}'.format(host,
                                           reverse('paypal-ipn')),
        'return_url': 'http://{}{}'.format(host,
                                           reverse('payment:done')),
        'cancel_return': 'http://{}{}'.format(host,
                                              reverse('payment:cancelled')),
    }
    key = settings.STRIPE_PUBLISHABLE_KEY
    form = PayPalPaymentsForm(initial=paypal_dict)
    return render(request, 'process_payment.html', {'order': order, 'form': form, 'key': key})


stripe.api_key = settings.STRIPE_SECRET_KEY


def charge(request):
    if request.method == 'POST':
        # charge = stripe.Charge.create(
        #     amount=500,
        #     currency='inr',
        #     description='A Django charge',
        #     source=request.POST['stripeToken']
        # )
        return render(request, 'payment_done.html', )
