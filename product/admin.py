from django.contrib import admin
from .models import Category, Product, Review
from django.contrib.sessions.models import Session


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'created_at', 'updated_at']


class ReviewAdmin(admin.ModelAdmin):
    list_display = ['user', 'product', 'star',
                    'comment', 'created_at', 'updated_at']


class ProductAdmin(admin.ModelAdmin):
    list_display = ['code',  'name', 'price',
                    'available',  'created_at', 'updated_at', 'category']


class SessionAdmin(admin.ModelAdmin):
    def _session_data(self, obj):
        return obj.get_decoded()
    list_display = ['session_key', '_session_data', 'expire_date']


admin.site.register(Session, SessionAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Review, ReviewAdmin)