from django import forms
from .models import Review


class ReviewForm(forms.ModelForm):
    class Meta:
        model = Review
        exclude = ('user', 'product')
        fields = ('star', 'comment')
        unique_together = ('product', 'user')
