from django.contrib.auth.models import User
from django.db.models import Avg
from django.urls import reverse
from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('product:product_list_by_category', args=[self.id])

    class Meta:
        db_table = 'Category'
        ordering = ('name', )


class Product(models.Model):
    name = models.CharField(max_length=100)
    code = models.IntegerField()
    image = models.ImageField(upload_to='product/')
    price = models.DecimalField(decimal_places=0, max_digits=6)
    description = models.TextField(blank=True)
    available = models.BooleanField(default=True)
    stock = models.PositiveIntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'Product'
        ordering = ('name',)

    def get_absolute_url(self):
        return reverse('product:product_detail', args=[self.id])


class Review(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    star = models.DecimalField(decimal_places=1, max_digits=2)
    comment = models.CharField(max_length=500)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.product.name

    def average(self, productQuery):
        productCodeList = [productQuery['code']
                           for productQuery in productQuery]
        averageStarList = {}
        for productCode in productCodeList:
            averageStar = Review.objects.filter(
                product=productCode).aggregate(avg=Avg('star'))
            averageStarList[productCode] = averageStar['avg']
        return averageStarList

    class Meta:
        unique_together = ('product', 'user',)
