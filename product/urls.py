from django.urls import path
from . import views

app_name = 'product'

urlpatterns = [
    path('', views.product_list, name='product_list'),
    path('productdetail/<int:code>/', views.product_detail, name='product_detail'),
    path('productreview/<int:code>/', views.product_review, name='product_review'),
    path('review/', views.product_review_RD,
         name='product_review_RD')
]
