from django.db.models import Avg, Max, Min
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render

from .formreview import ReviewForm
from .models import Product, Review


def product_list(request):
    req = request.GET
    filter_values = {}
    filter_star = {}
    searchterm = ''

    if len(req) > 0:
        if 'price-min' in req:
            filter_values['price__gte'] = req['price-min']

        if 'price-max' in req:
            filter_values['price__lte'] = req['price-max']

        if 'search' in req:
            filter_values['name__icontains'] = req['search']
            searchterm = req['search']

        if 'available' in req:
            available = req['available']
            if available == 'available':
                filter_values['available'] = True

        if 'star' in req:
            if req['star'] == '0':
                pass
            else:
                filter_star['review__star__avg__gte'] = req['star']

        products = Product.objects.filter(
            **filter_values).annotate(Avg('review__star')).filter(**filter_star)
    else:
        products = Product.objects.annotate(Avg('review__star'))

    product_minmax_price = Product.objects.all().aggregate(
        maximum_price=Max('price')+1, minimum_price=Min('price')-1)
    templateData = {'products': products, 'searchterm': searchterm,
                    'product_minmax_price': product_minmax_price}
    return render(request, 'home.html', templateData)


def product_detail(request, code, *args):
    filter_star = {}

    if 'starfilterpd' in request.GET:
        filter_star['star__gte'] = request.GET['starfilterpd']

    product = Product.objects.filter(code=code).annotate(
        Avg('review__star')).first()

    review = Review.objects.filter(product__code=code, **filter_star).values(
        'user', 'product', 'star', 'comment', 'created_at', 'updated_at', 'user__username', 'product__code')

    if 'cart' in request.session:
        if str(code) in request.session['cart'].keys():
            in_cart = True
        else:
            in_cart = False
    else:
        in_cart = False

    templateData = {'product': product, 'review': review, 'in_cart': in_cart}
    return render(request, 'productdetail.html', templateData,)


def product_review(request, code):
    productid = Product.objects.filter(code=code).values('id').first()
    if request.method == 'POST':
        form = ReviewForm(request.POST)
        if form.is_valid():
            order = form.save(commit=False)
            order.user_id = request.user.id
            order.product_id = productid['id']
            order = form.save()
            return redirect('accounts:pastorder')
    else:
        form = ReviewForm()
    templateData = {'form': form, 'code': code}
    return render(request, 'productreview.html', templateData)


def product_review_RD(request):
    req = request.GET

    if request.method == 'GET' and 'action' in req:
        product = req['product']
        action = req['action']

        if action == 'delete':
            Review.objects.filter(product__code=product,
                                  user__username=request.user).delete()

        if action == 'edit':
            reviewedit = req['reviewedit']
            z = Review.objects.get(
                product__code=product, user__username=request.user)
            z.comment = reviewedit
            z.save()

    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
