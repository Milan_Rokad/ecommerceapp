from django.db import models


class SellerProfile(models.Model):
    brand_name = models.CharField(max_length=50, unique=True)
    email = models.EmailField(max_length=254, unique=True)
    pickup_location = models.CharField(max_length=255)
