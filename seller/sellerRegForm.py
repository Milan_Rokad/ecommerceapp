from django import forms
from .models import SellerProfile


class SellerProfileForm(forms.ModelForm):
    class Meta:
        model = SellerProfile
        exclude = ()
        fields = ('brand_name', 'email', 'pickup_location')
