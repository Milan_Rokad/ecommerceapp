from django.shortcuts import render
from .sellerRegForm import SellerProfileForm


def seller_home(request):
    if request.method == 'POST':
        form = SellerProfileForm(request.POST)
        if form.is_valid():
            order = form.save() 
    else:
        form = SellerProfileForm()
    return render(request, 'seller-home.html', {'form': form})
